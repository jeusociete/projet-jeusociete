package servlets;

import models.Game;
import models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;



@WebServlet(name = "ServletGames")
public class ServletGames extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User currentUser = (User) request.getSession().getAttribute("user");

        String name = request.getParameter("name");
        String desc = request.getParameter("description");
        String minPlayers = request.getParameter("minPlayers");
        String maxPlayers = request.getParameter("maxPlayers");
        String theme = request.getParameter("theme");

        Game tmp = new Game(name, Integer.parseInt(minPlayers),Integer.parseInt(maxPlayers),desc, theme);
        currentUser.addGame(tmp);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doDelete(req, resp);

        String name = req.getParameter("name");
        User currentUser = (User) req.getSession().getAttribute("user");
        currentUser.removeGame(name);
    }
}
