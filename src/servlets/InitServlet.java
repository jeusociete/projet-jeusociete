package servlets;

import models.*;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

public class InitServlet extends HttpServlet {

    public static final String CONTEXT_EVENTS = "CONTEXT-EVENTS";
    public static final String CONTEXT_BASEUSER = "CONTEXT_BASEUSER";
    public static final String CONTEXT_BASEGAME = "CONTEXT_BASEGAME";

    @Override
    public void init(ServletConfig config) throws ServletException {


        //Create default test data

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        BaseEvent events = new BaseEvent();
        BaseUser users = new BaseUser();

        User admin1 = new User("jean", "jean", true);
        User admin2 = new User("test", "test", true);

        User user1 = new User("lucas", "tasoeur", false);
        User user2 = new User("yoan", "viveledev", false);
        User user3 = new User("guillaume", "ilovejava", false);

        Game game1 = new Game("L'étrange noel de mr Jack L'éventreur", 4, 8,"apprenez à devenir un tueur en série agissant en toute discrétion lors de la période de l'année où l'on aimerait que tout les cadeaux qu'on reçoit ne soient pas des coups de scalpels dans le thorax", "horreur");
        Game game2 = new Game("Donjon et Dragon", 3, 10, "devenez le roi de la faille en battant le grand Cthulu en personne en agissant de concert avec une équipe soudée", "role");
        Game game3 = new Game("Monopoly", 3, 9, "Gagner de l'argent et dépenser le pour en gagner plus c'est comme ça que ça fonctionne", "plateau");
        Game game4 = new Game("21", 2,2, "si tu arrives à 21 tu peux poser les pieds sous la table", "des");
        Game game5 = new Game("Black Jack", 2,4, "La partie oppose tous les joueurs contre la banque. Le but est de battre le croupier sans dépasser 21", "cartes");
        Game game6 = new Game("Qui va payer la facture", 2,5, "Jeu créateur de conflit entre générations pour les petits et grands", "famille");


        Event event1 = new Event(LocalDateTime.parse("2019-12-02 12:30", formatter), 30, "Reunion des joueurs anonymes", user1);
        Event event2 = new Event(LocalDateTime.parse("2019-12-24 16:00", formatter), 120, "Pinata en famille", user2);
        Event event3 = new Event(LocalDateTime.parse("2019-12-30 15:00", formatter), 240, "La villageoise et ses variantes", admin1);

        List<Game> user1event2Games = Arrays.asList(new Game[]{game1, game2});

        Participant user1_ = new Participant(user1, 0);

        event2.addParticipant(user1_, user1event2Games);

        events.addEvent(event1);
        events.addEvent(event2);
        events.addEvent(event3);


        user1.addGame(game1);
        user1.addGame(game2);

        admin1.addGame(game3);
        admin1.addGame(game4);

        user2.addGame(game5);
        user2.addGame(game6);

        event1.addAvailableGame(user1.getOwnedGames().get(0));
        event1.addAvailableGame(game2);
        try {
            users.addUser(user1);
            users.addUser(user2);
            users.addUser(user3);

            users.addUser(admin1);
            users.addUser(admin2);

        } catch (Exception e) { System.out.print(e);}

        config.getServletContext().setAttribute(CONTEXT_EVENTS, events);
        config.getServletContext().setAttribute(CONTEXT_BASEUSER, users);

        BaseUser testUsers = (BaseUser) config.getServletContext().getAttribute(CONTEXT_BASEUSER);

        for (User user : testUsers) {
            System.out.println(user);
        }
        /*ArrayList<Game> UserGames = new ArrayList<Game>();

        String userFilePath = config.getInitParameter("user-path");
        if (userFilePath == null) {
            throw new IllegalArgumentException("Le paramètre d'initialisation [user-path] n'est pas renseigné");
        }
        String eventFilepath = config.getInitParameter("event-path");
        if (eventFilepath == null) {
            throw new IllegalArgumentException("Le paramètre d'initialisation [event-path] n'est pas renseigné");
        }
        String gameFilePath = config.getInitParameter("game-path");
        if (gameFilePath == null) {
            throw new IllegalArgumentException("Le paramètre d'initialisation [game-path] n'est pas renseigné");
        }

        // Chargement des jeux

        List<Game> games = new ArrayList<>();
        System.out.println("Début du chargement des jeux\n");
        try {
            List<String> lines = Files.readAllLines(Path.of(config.getServletContext()
                    .getRealPath(gameFilePath)));
            for (String line : lines) {
                try {
                    Game game = parseGameFromLine(line);
                } catch (Exception e) {
                    System.err.printf("Impossible de parser le jeu pour la ligne [%s]\n", line);
                }
            }
            System.out.printf("Chargement de %d jeux réalisé\n", games.size());
        } catch (IOException e) {
            throw new ServletException("Erreur à la lecture des jeux", e);
        }
        BaseGame baseGame = new BaseGame(games);
        // Sauvegarde du catalogue dans le scope "application" (ServletContext) sous la constante CONTEXT_CATALOGUE
        config.getServletContext().setAttribute(CONTEXT_BASEGAME, baseGame);

        UserGames.add(baseGame.getGame().get(0));
        System.out.print("bonjour");
        System.out.print(baseGame.getGame().get(0).getName());

        // Chargement des séances

        List<Event> events = new ArrayList<>();
        System.out.print("début du chargement des events\n");
        try {
            List<String> lines = Files.readAllLines(Path.of(config.getServletContext().getRealPath(eventFilepath)));
            for (String line : lines) {
                try {
                    Event event = parseEventFromLine(line);
                    events.add(event);
                } catch (Exception e) {
                    System.err.printf("Impossible de parser le produit pour la ligne [%s]\n", line);
                }
            }
            System.out.printf("Chargement de %d Events réalisé\n", events.size());
        } catch (IOException e) {
            throw new ServletException("Erreur à la lecture des événements", e);
        }
        // Sauvegarde des events dans le scope "application" (ServletContext) sous la constante CONTEXT_EVENTS
        config.getServletContext().setAttribute(CONTEXT_EVENTS, events);

        // Chargement des utilisateurs

        List<User> baseUsers = new ArrayList<>();
        System.out.print("début du chargement des utilisateur\n");
        try {
            List<String> lines = Files.readAllLines(Path.of(config.getServletContext().getRealPath(userFilePath)));
            for (String line : lines) {
                try {
                    User user = parseUserfromLine(line);
                    baseUsers.add(user);
                } catch (Exception e) {
                    System.err.printf("Impossible de parser l'utilisateur pour la ligne [%s]\n", line);
                }
            }
            System.out.printf("Chargement de %d utilisateurs réalisé\n", baseUsers.size());
        } catch (IOException e) {
            throw new ServletException("Erreur à la lecture des utilisateurs", e);
        }
        BaseUser baseUser = new BaseUser(baseUsers);
        // Sauvegarde de la liste d'utilisateurs de base
        // dans le scope "application" (ServletContext) sous la constante CONTEXT_CATALOGUE
        config.getServletContext().setAttribute(CONTEXT_BASEUSER, baseUser);

*/
    }
/*
    private User parseUserfromLine(String line) {
        String[] elements = line.split(",");

        String pseudo = elements[0];
        String password = elements[1];
        String admin = elements[2];
        Boolean isAdmin = "admin".equalsIgnoreCase(admin);

        return new User(pseudo, password, isAdmin);
    }

    private Event parseEventFromLine(String line) {

            String[] elements = line.split(",");

            String date = elements[0];
            String duration = elements[1];
            String title = elements[2];
            DateTimeFormatter formatter;
            LocalDateTime dateTime;
            Integer realDuration;

        try {
             formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
             dateTime = LocalDateTime.parse(date, formatter);
             realDuration = Integer.parseInt(duration);

        return new Event(dateTime, realDuration, title);
        } catch (Exception e) { System.out.print(e);
            return new Event( LocalDateTime.now(), 30, "Evenement par défaut" );}
    }


    private Game parseGameFromLine(String line){
        String[] elements = line.split(",");

        String theme = elements[0];
        String name = elements[1];
        String description = elements[2];
        String minPlayer = elements[3];
        String maxPlayer = elements[4];

        Integer mnPlayer = Integer.parseInt(maxPlayer);
        Integer mxPlayer = Integer.parseInt(minPlayer);

        return new Game(name, mnPlayer, mxPlayer, description, theme);
    }
*/

}

