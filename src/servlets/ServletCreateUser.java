package servlets;

import models.BaseUser;
import models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ServletCreateUser")
public class ServletCreateUser extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BaseUser users = (BaseUser) getServletContext().getAttribute("CONTEXT_BASEUSER");

        String name = request.getParameter("name");
        String password = request.getParameter("password");
        Boolean isAdmin = request.getParameter("isAdmin") != null;

        User newUser = new User(name, password, isAdmin);

        users.addUser(newUser);

        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
