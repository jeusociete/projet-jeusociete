package servlets;

import models.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebServlet(name = "ServletParticipate")
public class ServletParticipate extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //Get params and session variables
        User currentUser = (User) request.getSession().getAttribute("user");
        BaseEvent events = (BaseEvent) getServletContext().getAttribute("CONTEXT-EVENTS");
        String[] selectedGames =  request.getParameterValues("gamesToAdd");

        String eventTitle = request.getParameter("event");
        Integer minParticipants = Integer.parseInt(request.getParameter("minParticipants"));

        //Logique metier

        Participant toAdd = new Participant(currentUser, minParticipants);
        List<Game> games4Event = null;
        Event main = events.findByName(eventTitle);
        main.addParticipant(toAdd, games4Event);

        for(Game game : currentUser.getOwnedGames()){
         System.out.print(request.getParameter("gamesToAdd"));

        }

        //display acknowledgement message

        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doDelete(request, response);

        User currentUser = (User) request.getSession().getAttribute("user");
        BaseEvent events = (BaseEvent) getServletContext().getAttribute("CONTEXT-EVENTS");
        String eventTitle = request.getParameter("event");
        Event main = events.findByName(eventTitle);

        main.deleteParticipant(currentUser);

    }
}