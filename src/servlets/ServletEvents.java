package servlets;

import models.BaseEvent;
import models.Event;
import models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@WebServlet(name = "ServletEvents")
public class ServletEvents extends HttpServlet {

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        BaseEvent events = (BaseEvent) getServletContext().getAttribute("CONTEXT-EVENTS");
        User currentUser = (User) request.getSession().getAttribute("user");

        String date = request.getParameter("date");
        String time = request.getParameter("time");
        String duration = request.getParameter("duration");
        String title = request.getParameter("title");
        String myDateTime = date+" "+time;
        LocalDateTime dateTime = LocalDateTime.parse(myDateTime, this.formatter);

        //Create event object
        Event tmp = new Event(dateTime, Integer.parseInt(duration), title, currentUser );
        events.addEvent(tmp);

        request.setAttribute("defaultResponse", "événement bien créé");
        request.getRequestDispatcher("/home.jsp").forward(request, response);

    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doDelete(req, resp);

        BaseEvent events = (BaseEvent) getServletContext().getAttribute("CONTEXT-EVENTS");
        String date = req.getParameter("date");

        LocalDateTime dateTime = LocalDateTime.parse(date, this.formatter);
        events.removeEvent(dateTime);

    }
}
