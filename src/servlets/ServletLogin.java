package servlets;
import models.BaseUser;
import models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;


@WebServlet(name = "ServletLogin")
public class ServletLogin extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, NullPointerException {

        BaseUser users = (BaseUser) request.getServletContext().getAttribute("CONTEXT_BASEUSER");

        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String msg = "login/password incorrect";
        Boolean isAuth = false;

        if( users != null && users.getClientForLoginAndPassword(login, password)!= null){
            User user = users.getClientForLoginAndPassword(login, password);
            HttpSession session = request.getSession();
            session.setAttribute("user", user);
            if(user.getChangePassword().equals(0)){
                request.getRequestDispatcher("/FirstCo.jsp").forward(request,response);
            }
            request.setAttribute("connexionSuccess","<p>Bonjour, "+user.getPseudo()+", heureux de vous revoir ! </p>");

        } else {
            request.setAttribute("connexionError","<p>l'utilisateur ou le mot de passe renseigné est incorrect</p>");
        }
        request.getRequestDispatcher("/home.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
