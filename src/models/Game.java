package models;

public class Game {
	private String theme;
	private String name;
	private Integer minPlayers;
	private Integer maxPlayers;
	private String description;
	private String owner;
	
	public Game(String name, Integer minPlayers, Integer maxPlayers, String description, String theme) {
		
		this.name = name;
		this.description = description;
		this.minPlayers = minPlayers;
		this.maxPlayers = maxPlayers;
		this.theme = theme;
		
	}

	//Getters
	public String getName() {
		return name;
	}
	
	public String getDesc() {
		return description;
	}
	
	public Integer getMinPlayers() {
		return minPlayers;
	}
	
	public Integer getMaxPLayers() { return maxPlayers; }

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMinPlayers(Integer minPlayers) {
		this.minPlayers = minPlayers;
	}

	public void setMaxPlayers(Integer maxPlayers) {
		this.maxPlayers = maxPlayers;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}
}
