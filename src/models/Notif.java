package models;

import java.time.LocalDateTime;
import java.util.Date;

public class Notif {

    private Date dateCreation;
    private boolean isRead;
    private String type;
    private LocalDateTime eventDate;
    private Integer eventDuration;
    private String message;

    Notif(Event event){
        this.eventDate = event.getDate();
       this.eventDuration = event.getDuration();
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public boolean isRead() {
        return isRead;
    }

    public String getType() {
        return type;
    }

    public LocalDateTime getEventDate() {
        return eventDate;
    }

    public Integer getEventDuration() {
        return eventDuration;
    }

    public String getMessage() {
        return message;
    }
}
