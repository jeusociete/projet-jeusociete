package models;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Event {
	
	private LocalDateTime date;
	private List<Participant> maybeHere;
	private ArrayList<Game> availableGames;
	private Integer duration;
	private String title;
	private User creator;
	private Integer participants;
	
	public Event(LocalDateTime date, Integer duration, String title, User creator) {
		this.date = date;
		this.duration = duration;
		this.availableGames = new ArrayList<Game>();
		this.maybeHere = new ArrayList<Participant>();
		this.title = title;
		this.creator = creator;
		this.participants = 0;
	}
	
	public Boolean changeDate(LocalDateTime nextDate) {
		// if same day but different hour
		if(nextDate.isAfter(date)) {
			return false;
		} else {
			return true;
		}
	}
	
	public void addAvailableGame(Game toAdd) {
		availableGames.add(toAdd);
		
	}
	public void countGames() {
		//return each game by number
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public List<Participant> getPotentialPeople() {
		return maybeHere;
	}

	public void setPotentialPeople(List<Participant> participants) {
		this.maybeHere = participants;
	}

	public List<Game> getAvailableGames() {
		return availableGames;
	}

	public void setAvailableGames(ArrayList<Game> availableGames) {
		this.availableGames = availableGames;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void addParticipant(Participant userToAdd, List<Game> gamesToAdd){

		maybeHere.add(userToAdd);
		Predicate<Participant> conditionFullfilled = guy -> guy.participate(participants+1);
		participants = maybeHere.stream().filter(conditionFullfilled).collect(Collectors.toList()).size();

		gamesToAdd.forEach(g -> {
			availableGames.add(g);
		});
	}

	public void deleteParticipant(User toDel){
		List<Game> gamesToDel = findGameByOwner(toDel.getPseudo());
		for(Game item: gamesToDel){
			availableGames.remove(item);
		}
		Participant tmp = findByName(toDel.getPseudo());
		maybeHere.remove(tmp);
	}

	private List<Game> findGameByOwner(String owner){

		Predicate<Game> byOwner = game -> game.getOwner().equals(owner);
		List<Game> result = availableGames.stream().filter(byOwner).collect(Collectors.toList());
		return result;
	}

	private Participant findByName(String pseudo){
		Predicate<Participant> byName = item -> item.getPseudo().equals(pseudo);
		Participant result = maybeHere.stream().filter(byName).collect(Collectors.toList()).get(0);
		return result;
	}
}
