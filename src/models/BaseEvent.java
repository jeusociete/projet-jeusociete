package models;

import java.security.cert.CollectionCertStoreParameters;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class BaseEvent {
	private List<Event> events = new ArrayList<Event>();

	public BaseEvent(List<Event> events) {
		this.events = events;
	}

	public BaseEvent() {
	}

	public void deleteEvent(Event toDel) {
		events.remove(toDel);
	}

	public List<Event> getEvents() {
		return events;
	}

	public void setEvent(List<Event> seances) {
		this.events = seances;
	}

	public void addEvent(Event toAdd) {
		events.add(toAdd);
	}

	public void removeEvent(LocalDateTime dateRemove) {
		for (Event e : this.events) {
			if (e.getDate() == dateRemove) {
				this.events.remove(e);
			}
		}
	}

	public Event findByName(String title) {
		for (Event e : this.events) {
			if (e.getTitle().equals(title)) {
				return e;
			}
		}
		return null;
	}
}