package models;

import java.util.ArrayList;
import java.util.List;

public class BaseUser extends ArrayList<User> {

    List<User> clients = new ArrayList<User>();

    public BaseUser(List<User> clients) {
        this.clients = clients;
    }
    public BaseUser(){ }

    /**
     * Cherche un client dans la base client à partir de son login et password
     * @param login
     * @param password
     * @return le client trouvé ou null s'il n'existe pas
     */
    public User getClientForLoginAndPassword(String login, String password) {
        if (login == null || password == null) throw new IllegalArgumentException("Aucun des paramètres login et password ne doit être null");

        for (User client : clients) {
            if (client.getPseudo().equals(login) && client.getPassword().equals(password)) {
                System.out.print("User found: \n");
                System.out.print(client.getPassword());
                return client;
            }
        }
        return null;
    }

    public void addUser(User toAdd){
        try {
            this.clients.add(toAdd);
        } catch (Exception e) {
            System.out.print(e);
        }
    }

    @Override
    public int size() {
        return this.clients.size();
    }
}
