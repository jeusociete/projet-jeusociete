package models;

import java.util.ArrayList;
import java.util.List;

public class User {

	private String pseudo;
	private String password;
	private Boolean isAdmin;
	private Integer changePassword;

	private List<Game> ownedGames = new ArrayList<Game>();

	private List notifs = new ArrayList<Notif>();

	public User(String pseudo, String password, Boolean isAdmin) {
		this.password = password;
		this.pseudo = pseudo;
		this.isAdmin = isAdmin;
		changePassword = 0;
	}

	public void AddNotif(Event event) {
		Notif newNotif = new Notif(event);
		notifs.add(newNotif);
	}

	private void createUser() {
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
		changePassword++;
	}

	public Boolean getAdmin() {
		return isAdmin;
	}

	public void setAdmin(Boolean admin) {
		isAdmin = admin;
	}

	public List<Game> getOwnedGames() {
		return ownedGames;
	}

	public void setOwnedGames(List<Game> ownedGames) {
		this.ownedGames = ownedGames;
	}

	public Integer getNumberNotifs() {
		return this.notifs.size();
	}

	public List<Notif> getNotifs() {
		return this.notifs;
	}

	public void addGame(Game toAdd){
		toAdd.setOwner(pseudo);
		ownedGames.add(toAdd);
	}

	public void removeGame(String toDel){
		ownedGames.remove(ownedGames.stream().filter(c -> c.getName().equals(toDel)).findFirst());
	}

	public Integer getChangePassword() {
		return changePassword;
	}
}

