package models;

import java.util.List;

public class BaseGame {

    public List<Game> getGame() {
        return game;
    }

    public void setGame(List<Game> game) {
        this.game = game;
    }

    List<Game> game;

    public BaseGame(List<Game> games) {
        this.game = games;
    }

}
