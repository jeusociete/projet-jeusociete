package models;

public class Participant extends User {
    private Integer minParticipants;
    public Participant(String pseudo, String password, Boolean isAdmin, Integer minParticipants){
        super(pseudo, password, isAdmin);
        this.minParticipants = minParticipants;
    }

    public Participant(User currentUser, Integer minParticipants){
        super(currentUser.getPseudo(), currentUser.getPassword(), currentUser.getAdmin());
        this.minParticipants = minParticipants;
    }

    public Boolean participate (Integer newValue){
        return minParticipants >= newValue;
    }
}
