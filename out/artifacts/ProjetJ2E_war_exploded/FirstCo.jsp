<%--
  Created by IntelliJ IDEA.
  User: guillaume
  Date: 19/01/2020
  Time: 18:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Veuillez changer votre mot de passe:</title>
</head>
<body>
    <form action="ServletFirstCo" method="post">
        <input type="password" id="password" name = "password" placeholder="Mot de passe"/>
        <input type="password" id="confirm" placeholder="Confirmer" onchange="checkPw()"/>
        <button type="submit" id="submit" title="Valider">Valider</button>
    </form>

    <script type="application/javascript">
        function checkPw(){
            if(document.getElementById("password").value === document.getElementById("confirm").value){
                document.getElementById("submit").disabled = false;
            }
            else{
                document.getElementById("submit").disabled = true;
            }
        }
    </script>
</body>
</html>
