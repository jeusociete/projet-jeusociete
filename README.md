Projet réalisé par Lucas Florentin, Yoan Filipe et Guillaume Gouhier en B3 EPSI. Veuilllez vous baser sur la branche **TestMergeDev** pour lire le code.

-- Edit Prélivrable de dernière minute.

Tout d'abord nous vous demanderons de remarquer avec, nous osons l'espérer, de l'étonnement ,et au choix
du dégout ou de l'inspiration; que nous avons réalisé l'entièreté de notre application en SPA. 

Certaines Fonctionnalités ne sont pas disponibles vis à vis des besoins utilisateur :

- La partie Participation était presque aboutie malheureusement après plus d'une heure de recherche il semblerait que des modifications
sur lesquelles nous étions en train de travailler soient passée à la trappe notamment (GIT, tout un art) sur de la correction de bugs.
Entre autres, je pense à deux fonctions du modèle:  celle de suppression du jeu d'un utilisateur (User.removeGame)
et celle de recherche d'un participant par rapport à son nom que Guillaume avait effectué avec un Prédicat plutot qu'une boucle.

- La partie Notification a été créée au niveau modèle (fichiers Classes), 
Cependant nous n'avons pas pu aboutir à la création du Servlet et des implémentations dans la vue.

Bonne correction!