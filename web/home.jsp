<%@ page import="java.io.IOException" %>
<%@ page import="models.*" %>
<%@ page contentType="text/html;charset=iso-8859-1" language="java" %>
<%
    User currentUser = (User) session.getAttribute("user");
    BaseEvent events = (BaseEvent) getServletConfig().getServletContext().getAttribute("CONTEXT-EVENTS");
%>

<!DOCTYPE html>
<html>
<meta charset="utf-8">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
          crossorigin="anonymous">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="header.css">
    <link rel="stylesheet" href="home.css">

    <div class="header">
        <a href="#home" class="logo">Logo</a>
        <div class="header-right">
            <a href="#">Home</a>
            <%
                if (currentUser==null){
            %>
            <a onclick="document.getElementById('formCo').style.display='block'" style="width:auto;">Login</a>
            <%
             } else {
            %>
            <a href="ServletDeco">Logout</a>
            <%
                }
                if(currentUser != null && currentUser.getAdmin()){%>
            <a onclick="document.getElementById('formCreateUser').style.display='block'" style="width:auto;">Add User</a>
            <a onclick="document.getElementById('formCreateEvent').style.display='block'" style="width:auto;">Create Event</a>
            <%
                }
            %>
            <a href="#">Profil</a>
            <a href="#" class="notification">
                <span>Inbox</span>

                <span class="badge"><%
                    if(currentUser != null) out.print(currentUser.getNumberNotifs()); %>
                </span>

            </a>
        </div>
    </div>

    <title>home</title>
</head>

<body>
<!-- __________________________ FORM FOR NOTIFICATIONS _________________________-->
<div id="Notif" class="modal">

    <form class="modal-content animate" action=# method="post">
        <div class="imgcontainer">
            <span onclick="document.getElementById('Notif').style.display='none'" class="close" title="Close Modal">&times;</span>
        </div>

        <div class="container">
            <br>
            <ul class="w3-ul w3-card-4">
                <%
                    try {
                        for (Notif item : currentUser.getNotifs()) { %>
                <li class="w3-bar"><% out.print(item.getMessage()); %>
                    <span onclick="this.parentElement.style.display='none'"
                          class="w3-bar-item w3-button w3-white w3-xlarge w3-right">�</span>
                </li>
                <%
                        }
                    } catch (NullPointerException e) {

                    }
                %>
                </>
            </ul>
        </div>

    </form>
</div>

<%
    if(request.getAttribute("defaultResponse")!=null){
        out.print(" <div class='alert alert-success' role = 'alert' >");
        out.print(request.getAttribute("defaultResponse"));
        out.print("</div >");
    }
    if(request.getAttribute("connexionError")!=null) {
    out.print(" <div class='alert alert-danger' role = 'alert' >");
    out.print(request.getAttribute("ConnexionError"));
    out.print("</div >");
}
    if(request.getAttribute("connexionSuccess")!=null) {
        out.print(" <div class='alert alert-success' role = 'alert' >");
        out.print(request.getAttribute("connexionSuccess"));
        out.print("</div >");
    }
    if(request.getAttribute("DecoMessage")!=null){
        out.print(" <div class='alert alert-success' role = 'alert' >");
        out.print(request.getAttribute("DecoMessage"));
        out.print("</div");
    }
%>

<!-- _________________________ FORM FOR LOGIN+ADD USER _________________________-->
<div id="formCo" class="modal">

    <form class="modal-content animate" action="ServletLogin" method="post">
        <div class="imgcontainer">
            <span onclick="document.getElementById('formCo').style.display='none'" class="close" title="Close Modal">&times;</span>
            <img src="img/img_avatar2.png" alt="Avatar" class="avatar">
        </div>

        <div class="container">
            <label><b>Username</b></label>
            <input type="text" placeholder="Enter Username" name="login" required>

            <label><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="password" required>

            <button type="submit">Login</button>
            <label>
                <input type="checkbox" checked="checked" name="remember"> Remember me
            </label>
        </div>

        <div class="container" style="background-color:#f1f1f1">
            <button type="button" onclick="document.getElementById('formCo').style.display='none'" class="cancelbtn">Cancel</button>
            <span class="psw">Forgot <a href="#">password?</a></span>
        </div>
    </form>
</div>
<script>
    // Get the modal
    var modal = document.getElementById('formCo');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
<!-- _________________________ BUTTON DOWL TO LIST EVENT _________________________-->

<a href="#list-events"><button type="button" class="btn btn-block">Liste des �v�nements</button></a>

<!--  ________________________________ CAROUSEL __________________________________-->

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

        <div class="item active">
            <img src="img/ny.jpg" alt="New york" style="width:100%;height: auto;">
        </div>

        <div class="item">
            <img src="img/chicago.jpg" alt="Chicago" style="width:100%;height: auto;">
        </div>

        <div class="item">
            <img src="img/la.jpg" alt="Los Angeles" style="width:100%; height: auto;">
        </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
    </a>
</div>


<!--  _______________________________ LIST EVENTS _________________________________-->
<script>
    var timerlen = 5;
    var slideAniLen = 250;

    var timerID = new Array();
    var startTime = new Array();
    var obj = new Array();
    var endHeight = new Array();
    var moving = new Array();
    var dir = new Array();

    function slidedown(objname){
        if(moving[objname])
            return;

        if(document.getElementById(objname).style.display != "none")
            return; // cannot slide down something that is already visible

        moving[objname] = true;
        dir[objname] = "down";
        startslide(objname);
    }

    function slideup(objname){
        if(moving[objname])
            return;

        if(document.getElementById(objname).style.display == "none")
            return; // cannot slide up something that is already hidden

        moving[objname] = true;
        dir[objname] = "up";
        startslide(objname);
    }

    function startslide(objname){
        obj[objname] = document.getElementById(objname);

        endHeight[objname] = parseInt(obj[objname].style.height);
        startTime[objname] = (new Date()).getTime();

        if(dir[objname] == "down"){
            obj[objname].style.height = "1px";
        }

        obj[objname].style.display = "block";

        timerID[objname] = setInterval('slidetick(\'' + objname + '\');',timerlen);
    }

    function slidetick(objname){
        var elapsed = (new Date()).getTime() - startTime[objname];

        if (elapsed > slideAniLen)
            endSlide(objname)
        else {
            var d =Math.round(elapsed / slideAniLen * endHeight[objname]);
            if(dir[objname] == "up")
                d = endHeight[objname] - d;
            obj[objname].style.height = d + "px";
        }
        return;
    }

    function endSlide(objname){
        clearInterval(timerID[objname]);

        if(dir[objname] == "up")
            obj[objname].style.display = "none";

        obj[objname].style.height = endHeight[objname] + "px";

        delete(moving[objname]);
        delete(timerID[objname]);
        delete(startTime[objname]);
        delete(endHeight[objname]);
        delete(obj[objname]);
        delete(dir[objname]);

        return;
    }
</script>

<div id="list-events"  class="container" >
    <h2>Events</h2>
    <%
        for (Event item : events.getEvents()) {

            System.out.print(item.getTitle());
    %>
    <div class="jumbotron">
        <h2 class="display-4"><% out.print(item.getTitle());%></h2>
        <p class="lead"> <% out.print(item.getDate()); %></p>
        <hr class="my-4">
        <p> <%item.getAvailableGames().size();%></p>
        <div class="container">
            <a role="button" class="btn btn-primary btn-lg" href="javascript:;" onmousedown="slidedown('<%out.print(item.getTitle());%>');">Voir plus</a>
            <a role="button" class="btn btn-primary btn-lg" href="javascript:;" onmousedown="slideup('<%out.print(item.getTitle());%>');">Voir moins</a>
        </div>

        <div id="<%out.print(item.getTitle());%>" class="container">
            <h2>Description</h2>

             <%
                 out.print("<p>"+item.getTitle()+"</p>");
                 out.print("<p>"+item.getAvailableGames().size()+"<p>");

                 for(User participant: item.getPotentialPeople()){
                     out.print("<p>"+participant.getPseudo()+" ramene:</p>");
                     for(Game game: item.getAvailableGames()){
                         if(game.getOwner().equals(participant.getPseudo())){
                             out.print("<p>Nom: "+game.getName()+"</p>");
                             out.print("<p>Description: "+game.getDesc()+"</p>");
                             out.print("<p>Min players: "+game.getMinPlayers()+"</p>");
                             out.print("<p>Max players: "+game.getMaxPLayers()+"</p>");
                         }
                     }
                 }
             %>
        </div>
        <button class="btn btn-primary" onclick="selectEventToParticipate('<%out.print(item.getTitle());%>')" style="width:auto;">Participer</button>
    </div>
        <%
            }
        %>

    </div>

<!-- MODAL CREATE USER (WHEN ADMIN) -->

<% if(currentUser != null && currentUser.getAdmin()){%>
<div id="formCreateUser" class="modal">

    <form class="modal-content animate" action="ServletCreateUser" method="post">
        <div class="imgcontainer">
            <span onclick="document.getElementById('formCreateUser').style.display='none'" class="close" title="Close Modal">&times;</span>
            <img src="img/img_avatar2.png" alt="Avatar" class="avatar">
        </div>

        <div class="container">
            <label><b>Username</b></label>
            <input type="text" placeholder="Enter Username" name="name" required>

            <label><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="password" required>
            <label>
                <input type="checkbox" checked="checked" name="isAdmin"> d�finir administrateur
            </label>
            <button type="submit">Login</button>

        </div>

        <div class="container" style="background-color:#f1f1f1">
            <button type="button" onclick="document.getElementById('formCreateUser').style.display='none';" class="cancelbtn">Cancel</button>
            <span class="psw">Forgot <a href="#">password?</a></span>
        </div>
    </form>
</div>
<%
}
%>

<!-- MODAL JOIN PARTICIPATION EVENT -->

<div id="formParticipate" class="modal">

    <form class="modal-content animate" action="ServletParticipate" method="post">
        <div class="imgcontainer">
            <span onclick="document.getElementById('formParticipate').style.display='none'" class="close" title="Close Modal">&times;</span>
        </div>

        <div id="thisIsTheEvent" class="container">

            <h2 id="titleEventSelected"></h2>
                <br>
                <h6>Participants</h6>
                    <ul id="participantsSelectedEvent">
                    </ul>
                <br>
            <%
                if(currentUser != null){
                    %>
                <select name="gamesToAdd" class="game" multiple>
                    <%
                    for(Game game : currentUser.getOwnedGames()){ %>
                        <option value="<%out.print(game.getName());%>">
                            <% out.print(game.getName());%>
                        </option>
                <%
                    } %>
                </select>
                    <%
                }
            %>
                 </select>
                <p>press <b>ctrl</b> to multiple select</p>

        </div>

        <div class="container">
            <h3> Seuil requis de participants pour valider votre pr�sence</h3>
            <select name="minParticipants">
                <option>0</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
            </select>
        </div>

        <div>
            <button type="submit" class="btn btn-primary" > Participer</button>
        </div>
      <%  if(currentUser!=null){ %>
        <div style="background-color:#f1f1f1">
            <button type="button" onclick="document.getElementById('formParticipate').style.display='none'" class="cancelbtn">Cancel</button>
        </div>
       <%
         }
        %>
    </form>
</div>

<script>
    // Get the modal participation
    let modalParticipate = document.getElementById('formParticipate');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modalParticipate) {
            modalParticipate.style.display = "none";
            }

        if(modalParticipate.style.display == "none"){
            while (document.getElementById("participantsSelectedEvent").firstChild) {
                document.getElementById("participantsSelectedEvent")
                    .removeChild(document.getElementById("participantsSelectedEvent").firstChild);
            }
        }
    }
    // instanciate the value of the Event selected
    let selectedEvent = [];

    // function to display datas from seleted event on modal participation
    function selectEventToParticipate(eventName){

        document.getElementById('formParticipate').style.display='block';

            <% for(Event e: events.getEvents()){ %>
                let _<%out.print(e.hashCode());%> = [];
                    if(eventName === '<% out.print(e.getTitle());%>'){
                        <%
                            for(Participant p: e.getPotentialPeople()){
                        %>

                        document.getElementById("titleEventSelected").innerHTML =
                            '<input name="event" type=text disabled=\'disabled\' value="<%out.print(e.getTitle());%>">';

                        let node = document.createElement("li");
                        let pseudoParticipant = document.createTextNode('<%out.print(p.getPseudo());%>');
                        node.appendChild(pseudoParticipant);
                        document.getElementById("participantsSelectedEvent").appendChild(node);
                        <%
                            };
                        %>
                    }
                <%
                 };
                 %>
        }
</script>

<!--  MODAL FOR CREATING EVENT -->

<div id="formCreateEvent" class="modal">

    <form class="modal-content animate" action="ServletEvents" method="post">
        <div class="imgcontainer">
            <span onclick="document.getElementById('formCreateEvent').style.display='none'" class="close" title="Close Modal">&times;</span>

        <h1>Cr�er votre �v�nement</h1>
        <form action="/ServletEvents">
            <label>Nom event</label>
            <input type="text" placeholder="nom de l'event" style="width: 350px" name="title">
            <label>Date:</label>
            <input type="date" placeholder="date" name="date">
            <label>Time:</label>
            <input type="time" placeholder="date" name="time">
            <label>Dur�e:</label>
            <input type="number" placeholder="Dur�e" name="duration">
        </form>

        <div>
            <button type="submit" class="btn btn-primary" > Cr�er �v�nement </button>
        </div>
        <div style="background-color:#f1f1f1">
            <button type="button" onclick="document.getElementById('formCreateEvent').style.display='none'" class="cancelbtn">Cancel</button>
        </div>
        </div>
    </form>
</div>


</body>
</html>